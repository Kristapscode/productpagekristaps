
function CheckFields(){
    //Checks sku input value
    let SKU=document.getElementById("sku").value;
    if (SKU=="")
    {
         document.getElementById("ErrorMessageSKU").innerHTML="Please, submit required data";
    }
    else{
     document.getElementById("ErrorMessageSKU").innerHTML="";
    
    }
 //Checks name input value
    let name=document.getElementById("name").value;
    if (name=="")
    {
         document.getElementById("ErrorMessageName").innerHTML="Please, submit required data";
         
    }
    else{
     document.getElementById("ErrorMessageName").innerHTML="";
     
    }
 //Checks sku price value
    let price=document.getElementById("price").value;
   if (isNaN(price)){
    document.getElementById("ErrorMessagePrice").innerHTML="Please, provide the data of indicated type"

    }
    if (price < 0){
    document.getElementById("ErrorMessagePrice").innerHTML="Please, provide the data of indicated type"

    }
    else if (price=="")
    {
         document.getElementById("ErrorMessagePrice").innerHTML="Please, submit required data";

    }
   else{
     document.getElementById("ErrorMessagePrice").innerHTML="";

   }
 //Checks size input value
 let size=document.getElementById("size").value;
   if (isNaN(size)){
    document.getElementById("ErrorMessageSize").innerHTML="Please, provide the data of indicated type";
    }
    else if (size=="")
    {
         document.getElementById("ErrorMessageSize").innerHTML="Please, submit required data";
    }
   else{
     document.getElementById("ErrorMessageSize").innerHTML="";
   }
   //Checks dimensions input values
   let height=document.getElementById("height").value;
   let width=document.getElementById("width").value;
   let length=document.getElementById("length").value;
 
   if (isNaN(height)||isNaN(width)||isNaN(length)){
    document.getElementById("ErrorMessageFurniture").innerHTML="Please, provide the data of indicated type";

    }
    else if (height==""||width==""||length=="")
    {
         document.getElementById("ErrorMessageFurniture").innerHTML="Please, submit required data";

    }
   else{
     document.getElementById("ErrorMessageFurniture").innerHTML="";
   }
   //Checks weight input value
   let weight=document.getElementById("weight").value;
   if (isNaN(weight)){
    document.getElementById("ErrorMessageWeight").innerHTML="Please, provide the data of indicated type";

    }
    else if (weight=="")
    {
         document.getElementById("ErrorMessageWeight").innerHTML="Please, submit required data";

    }
   else{
     document.getElementById("ErrorMessageWeight").innerHTML="";
   }
 }
 //runs CheckFields() every 100 miliseconds
 setInterval(CheckFields,100);