<!-- Including php scripts -->
<?php
error_reporting(0);
include './connection.php';
include './delete.php';
$result = $mysqli->query("SELECT * FROM product");
$rows = mysqli_fetch_array($result);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<link rel="stylesheet" href="style.css">   
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hi</title>
</head>
<body>
    <!-- Upper area with buttons -->
<nav class="navbar navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand">Product List</a>
    <div class="d-flex">
    <button type="button" onclick="window.location.href='./add-product.php'" class="btn btn-dark">ADD</button>
    <button type="submit" name="delete" form="all_cards" class="btn btn-dark">MASS DELETE</button>
</div>
  </div>
</nav>
<hr>
<!-- All product card container -->
<form id="all_cards" method="post">
<?php
foreach ($result as $rows){
?>
<!-- Individual cards, echoing data from mysql database Goods table products -->
<div id="card" >
    <?php
?>
<input class="delete-checkbox" type="checkbox" name="check[]" value="<?php echo $rows['id']; ?>" id="flexCheckDefault">
<?php 
echo "<p>{$rows['sku']}</p>";
 echo "<p>{$rows['name']}</p>";
 echo "<p>{$rows['price']}$</p>";
 if (!empty($rows['size']))
    echo "<p>Size: {$rows['size']}MB</p>";
if (!empty($rows['weight']))
    echo "<p>Weight: {$rows['weight']}KG</p>";
if (!empty($rows['height'])) 
    echo "<p>Dimensions: {$rows['height']}CM x {$rows['width']}CM x {$rows['length']}CM </p>";
?>
</div>
<?php  
}
?>
</form>
<footer class="ftr">
<hr>
<p class="bottom_text">Scandiweb test assignment</p>
</footer>

<!-- Scripts -->
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>    <script src="script.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
</body>
</html>