<!DOCTYPE html>
<html lang="en">
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<link rel="stylesheet" href="style.css">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Product Page</title>
</head>
<body>
<form action="insert_values.php" method="post" id="product_form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
<!-- Upper area with buttons -->
<nav class="navbar navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand">Product List</a>
    <div class="d-flex">
    <button type="submit" id="addbtn"name="submit" value="submit" onclick="CheckFields()" class="btn btn-dark">Save</button>
    <button type="button" onclick="window.location.href='index.php'" class="btn btn-dark">Cancel</button>
</div>
  </div>
</nav>
<hr>
<!-- SKU inputfield -->
<div class="input-group mb-3">
  <span class="input-group-text" id="inputGroup-sizing-default">SKU</span>
  <input id="sku" name="sku" type="text" value="" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
  <p id="ErrorMessageSKU"></p>
</div>
<!-- Name inputfield -->
  <div class="input-group mb-3">
  <span class="input-group-text" id="inputGroup-sizing-default">Name</span>
  <input id="name" name="name" type="text"  class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
  <p id="ErrorMessageName"></p>
</div>
<!-- Price inputfield -->
<div class="input-group mb-3">
  <span class="input-group-text" id="inputGroup-sizing-default">Price ($)</span>
  <input id="price" name="price" type="text"  class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
  <p id="ErrorMessagePrice"></p>
</div>
<!-- Selector -->
<select class="form-select" id="productType" aria-label="Default select example">

  <option onclick="NoneSelected()" selected>Open this menu to select product type</option>
  <option onclick="ShowInputDVD()" value="1" id="DVD">DVD</option>
  <option onclick="ShowInputFurniture()" value="2" id="Furniture">Furniture</option>
  <option onclick="ShowInputBook()" value="3" id="Book">Book</option>
</select>
<input type='hidden' id="selection" name="type" value=""/>
<!-- Size inputfield -->
<div id="DVDMB">
<div class="input-group mb-3">
  <span class="input-group-text" id="inputGroup-sizing-default">Size (MB)</span>
  <input type="text" name="size" value="" class="form-control" id="size" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"><br>
<p id="ErrorMessageSize"></p>
</div>
<p>Please provide DVD size in Megabytes</p>
</div>
<!-- Furniture inputfields -->
<div id="FurnitureDimensions">
<div class="input-group mb-3">
 <span class="input-group-text" id="inputGroup-sizing-default">Dimensions (HxWxL)(CM)</span>
 <input type="text"  class="form-control" name="height" id="height" placeholder="Height(CM)" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
 <input type="text"  class="form-control" name="width" id="width" placeholder="Width(CM)" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
 <input type="text"  class="form-control" name="length" id="length" placeholder="Length(CM)" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
 <br>
<p id="ErrorMessageFurniture"></p>
</div>
<p>Please provide the furniture dimensions in the corresponding fields in centimeters</p>
</div>
<!-- Weight inputfield -->
<div id="BookWeight">
<div class="input-group mb-3">
  <span class="input-group-text" id="inputGroup-sizing-default">Weight (KG)</span>
  <input type="text" name="weight" class="form-control" id="weight" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"><br>
  <p id="ErrorMessageWeight"></p>
</div>
<p>Please provide book weight in Kilograms</p>
</div>
</form>
<footer class="ftr">
<hr>
<p class="bottom_text">Scandiweb test assignment</p>
</footer>

<!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    <script src="script.js"></script>
    <script src="CheckFields.js"></script>
</body>
</html>